import main.IJob;
import main.Job;
import main.JobRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JobRunnerTest {
    JobRunner jobRunner;
    int nThreads;
    int nJobs;

    @BeforeEach
    void setUp() {
        nThreads = (int) (Math.random() * 100);
//        nThreads = 4;
        nJobs = (int) (Math.random() * 100);
//        nJobs = 2;
        ArrayList<IJob> jobArrayList = new ArrayList<>();
        for (int i = 0; i < nJobs; i++) {
//            Job job = new Job(i, Math.random() * 10000.0);
            Job job = new Job("Job " + i, 1000);
            jobArrayList.add(job);
        }
        jobRunner = new JobRunner(jobArrayList, nThreads);
        System.out.println("Starting test with " + nJobs + " jobs and " + nThreads + " threads.");
        jobRunner.executeJobs();
    }

    @Test
    void jobRunnerThreadsShouldBeEqualToFieldNThreads() {
        assertEquals(jobRunner.getnThreads(), nThreads);
    }

    @Test
    void numberOfJobsShouldBeEqualToFieldNJobs() {
        assertEquals(jobRunner.getJobArrayList().size(), nJobs);
    }

}