package main;

import concurrent.ConfigurableThreadPoolExecutor;
import concurrent.JobThread;
import server.HTTPJobRunnerServer;

import java.util.ArrayList;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class JobRunner {
    ArrayList<IJob> jobArrayList = new ArrayList<>();
    ThreadPoolExecutor threadPool;
    int nThreads;

    public JobRunner(ArrayList<IJob> jobArrayList, int nThreads) {
        this.jobArrayList = jobArrayList;
        this.nThreads = nThreads;;
    }

    public boolean executeJobs() {
        this.threadPool = new ConfigurableThreadPoolExecutor(nThreads);
        for (IJob job : jobArrayList) {
            JobThread jobThread = new JobThread(job);
            threadPool.submit(jobThread);
        }
        while (threadPool.getActiveCount() != 0 && !threadPool.getQueue().isEmpty());
        jobArrayList.clear();
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    public int getnThreads() {
        return nThreads;
    }

    public void setnThreads(int nThreads) {
        this.nThreads = nThreads;
    }

    public ArrayList<IJob> getJobArrayList() {
        return jobArrayList;
    }

    public static void main(String[] args) {
        int nThreads = (int) (Math.random() * 100);
//        nThreads = 4;
        int nJobs = (int) (Math.random() * 100);
//        nJobs = 2;
        ArrayList<IJob> jobArrayList = new ArrayList<>();
//        for (int i = 0; i < nJobs; i++) {
////            Job job = new Job(i, Math.random() * 10000.0);
//            Job job = new Job("Job " + i);
//            jobArrayList.add(job);
//        }

        JobRunner jobRunner = new JobRunner(jobArrayList, nThreads);
        HTTPJobRunnerServer httpJobRunnerServer = new HTTPJobRunnerServer(jobRunner);
        httpJobRunnerServer.startServer();
//        System.out.println("Starting test with " + nJobs + " jobs and " + nThreads + " threads.");
//        jobRunner.executeJobs();
    }

    public void addJob(IJob job) {
        jobArrayList.add(job);
    }
}
