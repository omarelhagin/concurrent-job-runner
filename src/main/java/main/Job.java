package main;

public class Job implements IJob {
    static int idCounter = 0;
    int id;
    String name;
    double executionTime;

    public Job(String name) {
        this.id = idCounter++;
        this.name = name;
        this.executionTime = 1000;
    }

    public Job(String name, double executionTimeInSeconds) {
        this.id = idCounter++;
        this.name = name;
        this.executionTime = executionTimeInSeconds * 1000;
    }

    @Override
    public void execute() {
        try {
            Thread.sleep((long) executionTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getExecutionTime() {
        return executionTime;
    }
}