package concurrent;

import main.IJob;
import main.Job;

import java.util.concurrent.*;

public class ConfigurableThreadPoolExecutor extends ThreadPoolExecutor {

    public ConfigurableThreadPoolExecutor(int nThreads) {
        super(nThreads, nThreads, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }

    @Override
    protected void afterExecute(Runnable runnable, Throwable throwable) {
        super.afterExecute(runnable, throwable);
        if (throwable == null && runnable instanceof Future) {
            try {
                Future<JobThread> future = (Future<JobThread>) runnable;
                if (future.isDone()) {
                    IJob jobResult = (IJob) future.get();
                    if (jobResult instanceof Job) {
                        System.out.println("Job " + ((Job) jobResult).getId() + " completed.");
                    }
                }
            } catch (CancellationException cancellationException) {
                throwable = cancellationException;
            } catch (ExecutionException executionException) {
                throwable = executionException.getCause();
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
        }
        if (throwable != null) {
            System.out.println(throwable.getClass().getName() + " in " +
                    ((Thread) runnable).getName() + "... Shutting down all jobs.");
            this.shutdownNow();
            throwable.printStackTrace();
        }
    }
}
