package concurrent;

import main.IJob;

import java.util.concurrent.Callable;

public class JobThread implements Callable {
    IJob job;

    public JobThread(IJob job) {
        this.job = job;
    }

    @Override
    public Object call() throws Exception {
        job.execute();
        return job;
    }
}
