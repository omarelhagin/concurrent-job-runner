package server;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.util.ServerRunner;
import main.Job;
import main.JobRunner;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

public class HTTPJobRunnerServer extends NanoHTTPD {

    private static final Logger LOG = Logger.getLogger(HTTPJobRunnerServer.class.getName());
    private JobRunner jobRunner;
    private String cors;
    private final static String ALLOWED_METHODS = "GET, POST, PUT, DELETE, OPTIONS, HEAD";
    private final static int MAX_AGE = 42 * 60 * 60;
    public final static String DEFAULT_ALLOWED_HEADERS = "origin,accept,content-type";
    public final static String ACCESS_CONTROL_ALLOW_HEADER_PROPERTY_NAME = "AccessControlAllowHeader";

    public HTTPJobRunnerServer(JobRunner jobRunner) {
        super(9100);
        this.jobRunner = jobRunner;
        this.cors = "*";
    }

    public void startServer() {
        ServerRunner.executeInstance(this);
    }

    @Override
    public Response serve(IHTTPSession session) {
        Map<String, String> header = session.getHeaders();
        Map<String, List<String>> params = session.getParameters();
        String uri = session.getUri();

        System.out.println(session.getMethod() + " '" + uri + "' ");

        Iterator<String> e = header.keySet().iterator();
        while (e.hasNext()) {
            String value = e.next();
            System.out.println("  HDR: '" + value + "' = '" + header.get(value) + "'");
        }
        e = params.keySet().iterator();
        while (e.hasNext()) {
            String value = e.next();
            List<String> paramsList = params.get(value);
            Iterator<String> paramsListIterator = paramsList.listIterator();
            while (paramsListIterator.hasNext()) {
                System.out.println("  PRM: '" + value + "' = '" + paramsListIterator.next() + "'");
            }
        }

        if (session.getMethod().equals(Method.GET)) {
            return newFixedLengthJSONResponse(Collections.unmodifiableMap(header),
                    getCurrentJobsAsJSON().toString());
        }

        if (session.getMethod().equals(Method.POST)) {
            if (uri.equals("/")) {
                return addJobAndReturnConfirmation(session);
            }

            if (uri.equals("/execute")) {
                return executeJobsAndReturnExecutionResult(session);
            }
        }

        return newFixedLengthJSONResponse(Collections.unmodifiableMap(header), "");
    }

    private Response newFixedLengthJSONResponse(Map<String, String> headers, String responseData) {
        Response response;
        response = NanoHTTPD.newFixedLengthResponse(Response.Status.OK, "application/json", responseData);
        response = addCORSHeaders(headers, response, cors);
        return response;
    }

    private Response addJobAndReturnConfirmation(IHTTPSession session) {
        Map<String, String> header = session.getHeaders();
        Map<String, String> sessionBody = new HashMap<>();
        try {
            session.parseBody(sessionBody);
        } catch (IOException ioException) {
            return newFixedLengthResponse(Response.Status.INTERNAL_ERROR,
                    MIME_PLAINTEXT,
                    "SERVER INTERNAL ERROR: IOException: " + ioException.getMessage());
        } catch (ResponseException responseException) {
            return newFixedLengthResponse(responseException.getStatus(),
                    MIME_PLAINTEXT,
                    responseException.getMessage());
        }

        JSONObject newJobJson = new JSONObject(session.getQueryParameterString());
        Job newJob = new Job(newJobJson.getString("name"),
                newJobJson.getDouble("executionTime"));
        jobRunner.addJob(newJob);
        JSONObject responseJson = new JSONObject();
        responseJson.append("message", "job added with id=" + newJob.getId());
        return newFixedLengthJSONResponse(Collections.unmodifiableMap(header),
                responseJson.toString());
    }

    private Response executeJobsAndReturnExecutionResult(IHTTPSession session) {
        Map<String, String> header = session.getHeaders();
        Map<String, String> sessionBody = new HashMap<>();
        try {
            session.parseBody(sessionBody);
        } catch (IOException ioException) {
            return newFixedLengthResponse(Response.Status.INTERNAL_ERROR,
                    MIME_PLAINTEXT,
                    "SERVER INTERNAL ERROR: IOException: " + ioException.getMessage());
        } catch (ResponseException responseException) {
            return newFixedLengthResponse(responseException.getStatus(),
                    MIME_PLAINTEXT,
                    responseException.getMessage());
        }

        JSONObject requestJson = new JSONObject(session.getQueryParameterString());
        jobRunner.setnThreads(requestJson.getInt("nThreads"));
        boolean executionCompleted = jobRunner.executeJobs();
        JSONObject responseJson = new JSONObject();
        responseJson.append("message", executionCompleted ? "All jobs completed." : "Jobs not completed");
        return newFixedLengthJSONResponse(Collections.unmodifiableMap(header),
                responseJson.toString());
    }

    private JSONArray getCurrentJobsAsJSON() {
        JSONArray jsonArray = new JSONArray(jobRunner.getJobArrayList());
        return jsonArray;
    }

    protected Response addCORSHeaders(Map<String, String> queryHeaders, Response resp, String cors) {
        resp.addHeader("Access-Control-Allow-Origin", cors);
        resp.addHeader("Access-Control-Allow-Headers", calculateAllowHeaders(queryHeaders));
        resp.addHeader("Access-Control-Allow-Credentials", "true");
        resp.addHeader("Access-Control-Allow-Methods", ALLOWED_METHODS);
        resp.addHeader("Access-Control-Max-Age", "" + MAX_AGE);

        return resp;
    }

    private String calculateAllowHeaders(Map<String, String> queryHeaders) {
        // here we should use the given asked headers
        // but NanoHttpd uses a Map whereas it is possible for requester to send
        // several time the same header
        // let's just use default values for this version
        return System.getProperty(ACCESS_CONTROL_ALLOW_HEADER_PROPERTY_NAME, DEFAULT_ALLOWED_HEADERS);
    }
}
